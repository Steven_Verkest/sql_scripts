CREATE DATABASE CursusDatabanken;

-- 0501
CREATE TABLE  Nummers (
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL, 
Artiest VARCHAR(100) CHAR SET utf8mb4 NOT NULL, 
Genre VARCHAR(50),
Jaar CHAR(4)
);

-- 0502
CREATE TABLE Huisdieren (
Naam varchar(100) CHAR SET utf8mb4 NOT NULL,
Leeftijd SMALLINT UNSIGNED NOT NULL,
Soort VARCHAR(50) NOT NULL
);

-- 0503
-- Hernoem je tabel Nummers naar Liedjes
RENAME TABLE Nummers to Liedjes;

-- 0504
-- Voeg een extra kolom, Album, toe aan Liedjes. 
-- Deze is niet verplicht, telt tot 100 internationale tekens.
 ALTER TABLE Liedjes
 ADD COLUMN Album CHAR CHAR SET utf8mb4 NOT NULL ;
 
-- 0505
-- Verwijder de kolom Genre voor Liedjes
ALTER TABLE Liedjes
DROP COLUMN Genre;

-- 0506
-- Voeg een extra kolom, Baasje, toe aan Huisdieren. 
-- Deze is verplicht, telt tot 100 internationale tekens

ALTER TABLE Baasje
ADD COLUMN Huisdieren CHAR CHAR SET utf8mb4 NOT NULL;

-- 0507
CREATE TABLE Metingen (
Tijdstip TIMESTAMP, -- dag en uur
Grootte INT, -- moet positief zijn tem 20000
Marge float(5,2) -- 3 tot, 2 na komma
);

-- 0508
-- Plaats volgende data in je tabel met muzieknummers:
-- 1. het nummer "John the Revelator" van de groep "Larkin Poe".
 -- Het album is "Peach" en het verschijningsjaar is 2017. 
 -- 2. het nummer "Missionary Man" van de groep "Ghost".
 -- Het album is "Popestar" en het verschijningsjaar is 2016.
 
  INSERT INTO Liedjes (nummer,groep,album,jaartal)
  VALUES("john the revelator","larkin poe","peach",2017),
  ("missionry","ghost","popestar",2016);
  
  
 
 